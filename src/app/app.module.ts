import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TarefasModule} from './tarefas';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    // RouterModule,
    // FormsModule,
    AppRoutingModule,
    TarefasModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
